local S = minetest.get_translator()

local function formspecme()
	return "size[10,12]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"textarea[1,0.25;5,0.5;test;Itemsearch;]"..
	"scroll_container[0,1;12,4;test;vertical;]"..
	"scrollbaroptions[min=1;max=120;arrows=hide;]"..
	"list[context;main;0,0;9,12;]"..
	"scroll_container_end[]"..
	"scrollbar[9,0.75;0.25,3.25;vertical;test;0.1]"..
	"list[current_player;craft;1,4.5;3,3;]"..
	"list[current_player;craftpreview;5,5.5;1,1;]"..
	"list[current_player;main;1,8;8,4;]"..
	"listring[current_player;craft]"..
	"listring[context;main]"..
	"listring[current_player;main]"
end

minetest.register_node("tase:me", {
	description = ("ME Block"),
	tiles = {"ME_ani.png^[verticalframe:32:2"},
	light_source = 5,
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("tase:dummy", {
	description = ("Dummy Block"),
	tiles = {"ME.png"},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		inv:set_size('main', 100)
	end,
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name())
		meta:set_string("formspec", formspecme())
	end,
	light_source = 5,
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})
